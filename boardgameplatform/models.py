import click
from flask.cli import with_appcontext
from sqlalchemy.sql import func
from . import db


class Player(db.Model):

    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String, nullable=False)
    surname = db.Column(db.String, nullable=False)
    elo = db.Column(db.Integer, nullable=False) #should be defined to be only positive?

    movements = db.relationship("Movement",
                                primaryjoin="Player.id==Movement.author_id",
                                back_populates="author")
    __table_args__ = (
        db.CheckConstraint(elo >= 0, name='elo_positive'),
        #db.CheckConstraint(str.isdigit(elo.__str__)),
        #db.CheckConstraint(elo.as_integer() == elo, name='elo_is_numeric'),
        {}
    )
    # swap to this? movements = db.relationship("Movement", back_populates="author")
    
    @staticmethod
    def get_schema():
        schema = {
            "type": "object",
            "required": ["name", "elo"]
        }
        props = schema["properties"] = {}
        props["name"] = {
            "description": "Player name",
            "type": "string"
        }
        props["surname"] = {
            "description": "Player surname",
            "type": "string"
        }
        props["elo"] = {
            "description": "player elo rank",
            "type": "number",
            "pattern": "[0-9]?[0-9]?[0-9]?[0-9]"
        }
        return schema

    
class Movement(db.Model):
    
    id = db.Column(db.Integer, primary_key=True, unique=True)
    timestamp_start = db.Column(db.DateTime, nullable=False) #Saves UTC0 time without timezone-info
    timestamp_end = db.Column(db.DateTime, nullable=False) #Saves UTC0 time without timezone-info
    move_number = db.Column(db.Integer, nullable=False) #is this first move? or third move? or seventh? Tells the turn number
    
    game = db.relationship("Game", back_populates="movements")
    game_id = db.Column(db.Integer,  db.ForeignKey("game.id"), nullable=False) #extracts game_id from game object
    
    author = db.relationship("Player")
    # swap to this? author = db.relationship("Player", back_populates="movements")
    author_id = db.Column(db.Integer, db.ForeignKey("player.id"), nullable=False) #author should be assigned to each move.

    # What if player is removed from db?
    pawn_positions_before = db.Column(db.String, nullable=False) # for easy implementation let's use csv like this
    #A8,B8,C8,D8,E8,F8,G8,H8 \n A7,B7,C7,D7,E7,F7,G7,H7 \n A6,B6,C6,D6,E6,F6,G6,H6 \n A5,B5,C5,D5,E5,F5,G5,H5 \n A4,B4,C4,D4,E4,F4,G4,H4 \n A3,B3,C3,D3,E3,F3,G3,H3 \n A2,B2,C2,D2,E2,F2,G2,H2 \n A1,B1,C1,D1,E1,F1,G1,H1 
    pawn_positions_after = db.Column(db.String, nullable=False)

    __table_args__ = (
        db.CheckConstraint(move_number >= 0, name='move_number_positive'),
        db.UniqueConstraint('move_number', 'game_id', name='_game_id_move_number_u_c'),
        #db.CheckConstraint(1==1, name='move_number_is_numeric'),
        {}
    )
    
    @staticmethod
    def get_schema():
        schema = {
            "type": "object",
            "required": [
                "timestamp_start",
                "timestamp_end",
                #"move_number", not required, set by db
                "game_id",
                "author_id",
                "pawn_positions_before", #possibly wouldn't be required, latest move for the game could be extracted from db. Let's keep for now
                "pawn_positions_after"
            ]
        }
        props = schema["properties"] = {}
        props["timestamp_start"] = {
            "description": "Timestamp from start of the movement",
            "type": "string",
            "pattern": "^[0-9]{4}-[01][0-9]-[0-3][0-9]T[0-9]{2}:[0-5][0-9]:[0-5][0-9]\\.[0-9]{6}Z$"
        }
        props["timestamp_end"] = {
            "description": "Timestamp from end of the movement",
            "type": "string",
            "pattern": "^[0-9]{4}-[01][0-9]-[0-3][0-9]T[0-9]{2}:[0-5][0-9]:[0-5][0-9]\\.[0-9]{6}Z$"
        }
        """
        props["move_number"] = {
            "description": "Movement number in the game",
            "type": "number"
        }
        """
        props["game_id"] = {
            "description": "Associated game id",
            "type": "number"
        }
        props["author_id"] = {
            "description": "Associated player id",
            "type": "number"
        }
        # We can use these unrestricted patterns for general purpose; it is client's responsibility to validate the logic
        props["pawn_positions_before"] = {
            "description": "Pawn positions before the move is executed",
            "type": "string"
        }
        # We can use these unrestricted patterns for general purpose; it is client's responsibility to validate the logic
        props["pawn_positions_after"] = {
            "description": "Pawn positions after the move is executed",
            "type": "string"
        }
        return schema

    @staticmethod
    def get_schema_chess():
        schema = Movement.get_schema()
        props = schema["properties"]
        #If we restric to chess only, we can use these patterns
        #one_element = "([wb][BQKNRS])"
        #one_row = "(([wb][BQKNRS])?,){7}([wb][BQKNRS])?\n"

        props["pawn_positions_before"] = {
            "description": "Pawn positions before the move is executed",
            "type": "string",
            "pattern": "((([wb][BQKNRS])?,){7}([wb][BQKNRS])?\n){8}"
        }
        props["pawn_positions_after"] = {
            "description": "Pawn positions after the move is executed",
            "type": "string",
            "pattern": "((([wb][BQKNRS])?,){7}([wb][BQKNRS])?\n){8}"
        }
        return schema
    
class Game(db.Model):

    id = db.Column(db.Integer, primary_key=True, unique=True)
    #game_type should be defined so that we can determine which schema we use to validate movement
    #let's keep this simple for now and assume chess only
    movements = db.relationship("Movement", back_populates="game")
    #game_id = db.Column(db.Integer,  db.ForeignKey("game.id"), nullable=False)

    def get_schema():
        schema = {
            "type": "object",
            "required": []
        }
        return schema
    

@click.command("init-db")
@with_appcontext
def init_db_command():
    db.create_all()
    return

@click.command("testgen")
@with_appcontext
def generate_test_data():
    import sys
    sys.path.append('../tests')  # append tests directory to path for importing   option1 
    sys.path.append('./tests')    # append tests directory to path for importing    option2
    from api_test_movement_only import _object_player, _object_movement, _chess_positions_start, _chess_positions_example_1st_move, _chess_positions_example_2nd_move

    game1 = Game()
    player1 = _object_player()
    player2 = _object_player()
    db.session.add_all([game1, player1, player2])
    db.session.commit()
    
    movement1 = _object_movement(player1.id, game1.id, _chess_positions_start(), _chess_positions_example_1st_move())
    db.session.add(movement1)
    db.session.commit()
    movement2 = _object_movement(player2.id, game1.id, _chess_positions_example_1st_move(), _chess_positions_example_2nd_move())
    db.session.add(movement2)
    db.session.commit()
    return

