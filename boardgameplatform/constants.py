MASON = "application/vnd.mason+json"

LINK_RELATIONS_URL = "/boardgameplatform/link-relations/"
ERROR_PROFILE = "/profiles/error/"
PLAYER_PROFILE = "/profiles/player/"
GAME_PROFILE = "/profiles/game/"
MOVEMENT_PROFILE = "/profiles/movement/"