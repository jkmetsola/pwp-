import json
from flask import Response, request, url_for
from .constants import *
from .models import *
#from .api import api
#from boardgameplatform.resources import game as gameresources
#from boardgameplatform.resources import player as playerresources
#from boardgameplatform.resources import movement as movementresources

class MasonBuilder(dict):
    """
    A convenience class for managing dictionaries that represent Mason
    objects. It provides nice shorthands for inserting some of the more
    elements into the object but mostly is just a parent for the much more
    useful subclass defined next. This class is generic in the sense that it
    does not contain any application specific implementation details.
    """

    def add_error(self, title, details):
        """
        Adds an error element to the object. Should only be used for the root
        object, and only in error scenarios.

        Note: Mason allows more than one string in the @messages property (it's
        in fact an array). However we are being lazy and supporting just one
        message.

        : param str title: Short title for the error
        : param str details: Longer human-readable description
        """

        self["@error"] = {
            "@message": title,
            "@messages": [details],
        }

    def add_namespace(self, ns, uri):
        """
        Adds a namespace element to the object. A namespace defines where our
        link relations are coming from. The URI can be an address where
        developers can find information about our link relations.

        : param str ns: the namespace prefix
        : param str uri: the identifier URI of the namespace
        """

        if "@namespaces" not in self:
            self["@namespaces"] = {}

        self["@namespaces"][ns] = {
            "name": uri
        }

    def add_control(self, ctrl_name, href, **kwargs):
        """
        Adds a control property to an object. Also adds the @controls property
        if it doesn't exist on the object yet. Technically only certain
        properties are allowed for kwargs but again we're being lazy and don't
        perform any checking.

        The allowed properties can be found from here
        https://github.com/JornWildt/Mason/blob/master/Documentation/Mason-draft-2.md

        : param str ctrl_name: name of the control (including namespace if any)
        : param str href: target URI for the control
        """

        if "@controls" not in self:
            self["@controls"] = {}

        self["@controls"][ctrl_name] = kwargs
        self["@controls"][ctrl_name]["href"] = href

class GameBuilder(MasonBuilder):

    def add_control_add_game(self):
        self.add_control(
            "brdgame:add-game",
            href=url_for("api.gamecollection"),
            method="POST",
            title="Add new game",
            schema=Game.get_schema()
        )

    def add_control_players_all(self):
        self.add_control(
            "brdgame:players-all",
            href=url_for("api.playercollection"),
            method="GET",
            title="Get all players"
        )

    def add_control_movements_all(self):
        self.add_control(
            "brdgame:movements-all",
            href=url_for("api.movementcollection"),
            method="GET",
            title="Get all movements"
        )
    """
    def add_control_games_by(self, player):
        self.add_control(
            "brdgame:games-by",
            href=url_for("api.gamesbyplayer", player=player),
            method="GET",
            title="Add new game"
        )
    """
    def add_control_delete(self, game):
        self.add_control(
            "brdgame:delete",
            href=url_for("api.gameitem", game=game),
            method="DELETE",
            title="Delete this game"
        )

    def add_control_self(self, game):
        self.add_control("self", href=url_for("api.gameitem", game=game))

    def add_control_up(self):
        self.add_control("up", href=url_for("api.gamecollection"))

    def add_control_profile(self):
        self.add_control("profile", href=GAME_PROFILE)

    def add_control_collection(self):
        self.add_control(
            "collection",
            href=url_for("api.gamecollection"),
            method="GET",
            title="List all games"
        )
    """
    def add_control_author(self, player):
        self.add_control(
            "author",
            href=url_for("api.playeritem", player=player),
            method="GET",
            title="Get author of the specified moves"
        )
    """

class PlayerBuilder(MasonBuilder):

    def add_control_add_player(self):
        self.add_control(
            "brdgame:add-player",
            href=url_for("api.playercollection"),
            method="POST",
            title="Add new player",
            schema=Player.get_schema()
        )

    def add_control_games_all(self):
        self.add_control(
            "brdgame:games-all",
            href=url_for("api.gamecollection"),
            method="GET",
            title="Get all games"
        )

    def add_control_movements_all(self):
        self.add_control(
            "brdgame:movements-all",
            href=url_for("api.movementcollection"),
            method="DELETE",
            title="Delete this player"
        )

    def add_control_delete(self, player):
        self.add_control(
            "brdgame:delete",
            href=url_for("api.playeritem", player=player),
            method="DELETE",
            title="Delete this player"
        )

    def add_control_self(self, player):
        self.add_control("self", href=url_for("api.playeritem", player=player))

    def add_control_up(self):
        self.add_control("up", href=url_for("api.playercollection"))

    def add_control_profile(self):
        self.add_control("profile", href=PLAYER_PROFILE)

    def add_control_collection(self):
        self.add_control(
            "collection",
            href=url_for("api.playercollection"),
            method="GET",
            title="List all players"
        )

    def add_control_edit(self, player):
        self.add_control(
            "edit",
            href=url_for("api.playeritem", player=player),
            method="PUT",
            encoding="json",
            title="Edit this player",
            schema=Player.get_schema()
        )

class MovementBuilder(MasonBuilder):

    def add_control_add_movement(self):
        self.add_control(
            "brdgame:add-movement",
            href=url_for("api.movementcollection"),
            method="POST",
            title="Add new movement",
            schema=Movement.get_schema_chess()
        )
   
    def add_control_games_all(self):
        self.add_control(
            "brdgame:games-all",
            href=url_for("api.gamecollection"),
            method="GET",
            title="Get all games"
        )

    def add_control_players_all(self):
        self.add_control(
            "brdgame:players-all",
            href=url_for("api.playercollection"),
            method="GET",
            title="Get all players"
        )

    """
    def add_control_movements_by(self, player):
        self.add_control(
            "brdgame:movements-by",
            href=url_for("api.movementsbyplayer", player=player),
            method="GET",
            title="Get movements authored by some player"
        )
    """
    """
    def add_control_movements_by(self, player, game):
        self.add_control(
            "brdgame:movements-by",
            #if we implement the original plan we would need if statements here to 
            #define origin dependent next hop. Now we have easy way out. Because we know the origin
            #href=url_for("api.self", player=player, self.game)
            href="{}{}".format(url_for("api.movementsingame", player=player, game=game)),
            method="GET",
            title="Get movements authored by some player"
        )
    """

    def add_control_movements_in(self):
        self.add_control(
            "brdgame:movements-in",
            #if we implement the original plan we would need if statements here to 
            #define origin dependent next hop. Now we have easy way out. Because we know the origin
            href=url_for(
                "api.movementsingame",
                game="game_id"
                ).replace("game_id","?{game_id}"),
            method="GET",
            title="Get movements in some specified game"
        )

    """
    def add_control_movements_by_in(self, game, player):

        self.add_control(
            "brdgame:movements-in",
            href=url_for("api.movementsingamebyplayer", player=player, game=game),
            method="GET",
            title="Get movements in some specified game authored by some specified player"
        )
    """
    def add_control_movements_in_by(self, game):

        self.add_control(
            "brdgame:movements-by",
            href=url_for(
                "api.movementsingamebyplayer",
                game=game,
                player="author_id"
                ).replace("author_id","?{author_id}"),
            method="GET",
            title="Get movements in some specified game authored by some specified player"
        )
    

    def add_control_game(self, movement):

        movement = Movement.query.filter(Movement.id == movement).first()
        game = movement.game_id
        self.add_control(
            "brdgame:game",
            href=url_for("api.gameitem", game=game),
            method="GET",
            title="Get game associated to this move"
        )

    def add_control_delete(self, movement):
        self.add_control(
            "brdgame:delete",
            href=url_for("api.movementitem", movement=movement),
            method="DELETE",
            title="Delete this movement"
        )
 
    def add_control_self(self, movement):
        self.add_control("self", href=url_for("api.movementitem", movement=movement))

    def add_control_up(self):
        self.add_control("up", href=url_for("api.movementcollection"))

    def add_control_profile(self):
        self.add_control("profile", href=MOVEMENT_PROFILE)

    def add_control_collection(self):
        self.add_control(
            "collection",
            href=url_for("api.movementcollection"),
            method="GET",
            title="List all movements"
        )

    def add_control_author(self, movement):
        movement = Movement.query.filter(Movement.id == movement).first()
        self.add_control(
            "author",
            href=url_for("api.playeritem", player=movement.author_id),
            method="GET",
            title="Get author of the specified move(s)"
        )

    def add_control_edit(self, movement):
        self.add_control(
            "edit",
            href=url_for("api.movementitem", movement=movement),
            method="PUT",
            encoding="json",
            title="Edit this movement",
            schema=Movement.get_schema_chess()
        )


def create_error_response(status_code, title, message=None):
    resource_url = request.path
    body = MasonBuilder(resource_url=resource_url)
    body.add_error(title, message)
    body.add_control("profile", href=ERROR_PROFILE)
    return Response(json.dumps(body), status_code, mimetype=MASON)