from flask import Blueprint
from flask_restful import Api

api_bp = Blueprint("api", __name__, url_prefix="/api")
api = Api(api_bp)

from .resources.game import GameCollection, GameItem#, GamesbyPlayer
from .resources.player import PlayerCollection, PlayerItem
from .resources.movement import MovementCollection, MovementItem, MovementsinGame, MovementsinGamebyPlayer#, MovementsbyPlayer

api.add_resource(GameCollection, "/games/")
api.add_resource(GameItem, "/games/<game>/")
#api.add_resource(GamesbyPlayer, "/api/games/<player>")

api.add_resource(PlayerCollection, "/players/")
api.add_resource(PlayerItem, "/players/<player>/")

api.add_resource(MovementCollection, "/movements/")
api.add_resource(MovementItem, "/movements/<movement>/")
#api.add_resource(MovementsbyPlayer, "/api/movements/<player>")
api.add_resource(MovementsinGame, "/movements/game/<game>/")
api.add_resource(MovementsinGamebyPlayer, "/movements/game/<game>/player/<player>/")

