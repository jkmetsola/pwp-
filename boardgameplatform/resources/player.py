
import json
from jsonschema import validate, ValidationError
from flask import Response, request, url_for
from flask_restful import Resource
from .. import db
from ..models import Player, Movement
from ..constants import *
from ..utils import PlayerBuilder, create_error_response

class PlayerCollection(Resource):
    def get(self):

        body = PlayerBuilder()
        body.add_namespace("brdgame", LINK_RELATIONS_URL)
        body.add_control_add_player()
        body.add_control_games_all()
        body.add_control_movements_all()
        body.add_control("self", url_for("api.playercollection"))
        body.add_control_profile()
        body.add_control_collection()
        body["items"] = []
        for player in Player.query.all():
            item = PlayerBuilder(
                id=player.id,
                name=player.name,
                surname=player.surname,
                elo=player.elo
            )
            item.add_control_delete(player.id)
            item.add_control_self(player.id)
            item.add_control_up()
            item.add_control_profile()
            item.add_control_edit(player.id)
            body["items"].append(item)

        return Response(
           status=200,
           content_type="application/vnd.mason+json",
           response=json.dumps(body)
        )
    def post(self):
        if not request.json:
            return create_error_response(415, "Invalid content type", message=None)

        try:
            validate(request.json, Player.get_schema())
            player = Player(
                name=str(request.json["name"]),
                surname=str(request.json["surname"]),
                elo=int(request.json["elo"])
            )

            db.session.add(player)
            db.session.commit()
            return Response(
                headers={
                    "Location": url_for("api.playeritem", player=player.id)
                },
                status=201
            )
        
        except ValidationError as e:
            return create_error_response(400, "Invalid JSON document", str(e))
    

class PlayerItem(Resource):
    def get(self, player):

        if not Player.query.filter(Player.id == player).first():
            return create_error_response(
                400, 
                "Invalid Request",
                "No such player id."
            )

        player = Player.query.filter(Player.id == player).first()

        body = PlayerBuilder()
        body.add_namespace("brdgame", LINK_RELATIONS_URL)
        body["id"] = player.id
        body["name"] = player.name
        body["surname"] = player.surname
        body["elo"] = player.elo
        
        body.add_control_delete(player.id)
        body.add_control_self(player.id)
        body.add_control_up()
        body.add_control_profile()
        body.add_control_edit(player.id)

        #print("products all body")
        #print(body)
        return Response(
           status=200,
           content_type="application/vnd.mason+json",
           response=json.dumps(body)
        )
    def put(self, player):

        if not Player.query.filter(Player.id == player).first():
            return create_error_response(
                400, 
                "Invalid Request",
                "No such player id."
            )

        player_obj = Player.query.filter(Player.id == player).first()

        if not request.json:
            return create_error_response(415, "Invalid content type", message=None)

        try:
            validate(request.json, Player.get_schema())
            player_obj.name=str(request.json["name"])
            player_obj.surname=str(request.json["surname"])
            player_obj.elo=int(request.json["elo"])

            # there's no restriction regarding the other players on db; players can have same names.
            body = PlayerBuilder(
                id=player_obj.id,
                name=player_obj.name,
                surname=player_obj.surname,
                elo=player_obj.elo
            )


            db.session.commit()
            return Response(
                status=204,
                content_type="application/vnd.mason+json",
                response=json.dumps(body)
            )
        
        except ValidationError as e:
            return create_error_response(400, "Invalid JSON document", message=str(e))

    def delete(self, player):

        if not Player.query.filter(Player.id == player).first():
            return create_error_response(404, "Player does not exist", message=None)
        for movement in Movement.query.filter(Movement.author_id == player):
            db.session.delete(movement)
        db.session.commit()
        player = Player.query.filter(Player.id == player).first()
        db.session.delete(player)
        db.session.commit()
        return "", 204
