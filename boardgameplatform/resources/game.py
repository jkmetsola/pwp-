
import json
from jsonschema import validate, ValidationError
from flask import Response, request, url_for
from flask_restful import Resource
from .. import db
from ..models import Game, Movement#, Player
from ..constants import *
from ..utils import GameBuilder, create_error_response

class GameCollection(Resource):
    def get(self):

        body = GameBuilder()
        body.add_namespace("brdgame", LINK_RELATIONS_URL)
        body.add_control_add_game()
        body.add_control_players_all()
        body.add_control_movements_all()
        body.add_control("self", href=url_for("api.gamecollection"))
        body.add_control_profile()
        body["items"] = []
        for game in Game.query.all():
            item = GameBuilder(
                game=game.id
            )
            item.add_control_delete(game.id)
            item.add_control_self(game.id)
            item.add_control_up()
            item.add_control_profile()
            item.add_control_collection()
            body["items"].append(item)

        #print("products all body")
        #print(body)
        return Response(
           status=200,
           content_type="application/vnd.mason+json",
           response=json.dumps(body)
        )
    def post(self):

        # empty json only (at least in this phase, keep it simple)
        if request.json != {}:
            return create_error_response(
                415,
                "Invalid content type",
                message="When posting game, json object should be empty i.e.: {}"
            )

        try:
            #validate(request.json, Game.get_schema())
            game = Game()
            db.session.add(game)
            db.session.commit()
            return Response(
                headers={
                    "Location": url_for("api.gameitem", game=game.id)
                },
                status=201
            )
        
        except ValidationError as e:
            return create_error_response(400, "Invalid JSON document", str(e))

class GameItem(Resource):
    def get(self, game):

        if not Game.query.filter(Game.id == game).first():
            return create_error_response(
                404, 
                "Invalid Request",
                "No such game id."
            )

        game = Game.query.filter(Game.id == game).first()
        body = GameBuilder()
        body.add_namespace("brdgame", LINK_RELATIONS_URL)
        body["id"] = game.id
        body.add_control_delete(game.id)
        body.add_control_self(game.id)
        body.add_control_up()
        body.add_control_profile()
        body.add_control_collection()

        return Response(
            status=200,
            content_type="application/vnd.mason+json",
            response=json.dumps(body),

        )
    def delete(self, game):
        if not Game.query.filter(Game.id == game).first():
            return create_error_response(
                404, 
                "Invalid Request",
                "No such game id."
            )
        for movement in Movement.query.filter(Movement.game_id == game):
            db.session.delete(movement)
        db.session.commit()
        game = Game.query.filter(Game.id == game).first()
        db.session.delete(game)
        db.session.commit()
        return "", 204


"""
class GamesbyPlayer(Resource):
    def init():
        return
"""
