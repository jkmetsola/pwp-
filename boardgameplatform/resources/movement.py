
import json
from datetime import datetime
from jsonschema import validate, ValidationError
from flask import Response, request, url_for
from flask_restful import Resource
from .. import db
from ..models import Game, Player, Movement
from ..constants import *
from ..utils import MovementBuilder, create_error_response

class MovementCollection(Resource):
    def get(self):

        body = MovementBuilder()
        body.add_namespace("brdgame", LINK_RELATIONS_URL)
        body.add_control_add_movement()
        body.add_control_games_all()
        body.add_control_players_all()
        body.add_control_movements_in()
        body.add_control("self", url_for("api.movementcollection"))
        body.add_control_profile()
        body["items"] = []

        for movement in Movement.query.all():
            item = MovementBuilder(
                id=movement.id,
                timestamp_start=str(movement.timestamp_start.isoformat() + "Z"),
                timestamp_end=str(movement.timestamp_end.isoformat() + "Z"),
                move_number=movement.move_number,
                game_id=movement.game_id,
                author_id=movement.author_id,
                pawn_positions_before=movement.pawn_positions_before,
                pawn_positions_after=movement.pawn_positions_after
            )
            item.add_control_game(movement.id)
            item.add_control_delete(movement.id)
            item.add_control_self(movement.id)
            item.add_control_up()
            item.add_control_profile()
            item.add_control_collection()
            #item.add_control_edit()
            item.add_control_author(movement.id)
            body["items"].append(item)

        #print("products all body")
        #print(body)
        return Response(
           status=200,
           content_type="application/vnd.mason+json",
           response=json.dumps(body)
        )
    def post(self):
        if not request.json:
            return create_error_response(415, "Invalid content type", message=None)

        try:
            validate(request.json, Movement.get_schema())
            movement = Movement(
                timestamp_start=datetime.fromisoformat(
                    str(request.json["timestamp_start"]).replace("Z", "+00:00") #isoformat YYYY-MM-DD[*HH[:MM[:SS[.fff[fff]]]][+HH:MM[:SS[.ffffff]]]]
                ),
                #move_number=int(request.json["move_number"]), 
                # to avoid client issues and simplify validation
                # lets assign move number based on number of moves found by the same game_id
                move_number=1+len(
                    Movement.query.filter(Movement.game_id == int(request.json["game_id"])).all()
                ),
                game_id=int(request.json["game_id"]),
                author_id=int(request.json["author_id"]),
                pawn_positions_before=str(request.json["pawn_positions_before"]),
                #for simplicity, let's determine client can't send post to server until the turn has ended.
                #We'll determie schema to validate this
                timestamp_end=datetime.fromisoformat(
                    str(request.json["timestamp_end"]).replace("Z", "+00:00") #isoformat YYYY-MM-DD[*HH[:MM[:SS[.fff[fff]]]][+HH:MM[:SS[.ffffff]]]]
                ),
                pawn_positions_after=str(request.json["pawn_positions_after"])

            )
            if movement.pawn_positions_before == movement.pawn_positions_after:
                return create_error_response(
                    400, 
                    "Invalid JSON document",
                    "Pawn positions after cannot be the same as before."
                )
            if movement.timestamp_start > movement.timestamp_end:
                return create_error_response(
                    400, 
                    "Invalid JSON document",
                    "Turn cannot end before it starts."
                )
            if not Game.query.filter(Game.id == movement.game_id).first():
                return create_error_response(
                    400, 
                    "Invalid JSON document",
                    "No such game id."
                )
            if not Player.query.filter(Player.id == movement.author_id).first():
                return create_error_response(
                    400, 
                    "Invalid JSON document",
                    "No such player id."
                )
            
            db.session.add(movement)
            db.session.commit()
            return Response(
                headers={
                    "Location": url_for("api.movementitem", movement=movement.id)
                },
                status=201
            )
        
        except ValidationError as e:
            return create_error_response(400, "Invalid JSON document", str(e))

class MovementItem(Resource):

    def get(self, movement):
        
        if not Movement.query.filter(Movement.id == movement).first():
                return create_error_response(
                    404, 
                    "Invalid Request",
                    "No such movement id."
                )

        movement = Movement.query.filter(Movement.id == movement).first()

        body = MovementBuilder()
        body.add_namespace("brdgame", LINK_RELATIONS_URL)
        body["id"]=movement.id
        body["timestamp_start"] = str(movement.timestamp_start.isoformat() + "Z")
        body["timestamp_end"] = str(movement.timestamp_end.isoformat() + "Z")
        body["move_number"] = int(movement.move_number)
        body["game_id"] = int(movement.game_id)
        body["author_id"] = int(movement.author_id)
        body["pawn_positions_before"] = str(movement.pawn_positions_before)
        body["pawn_positions_after"] = str(movement.pawn_positions_after)
        
        body.add_control_game(movement.id)
        body.add_control_delete(movement.id)
        body.add_control_self(movement.id)
        body.add_control_up()
        body.add_control_profile()
        #body.add_control_edit(movement.id)
        body.add_control_author(movement.id)

        return Response(
           status=200,
           content_type="application/vnd.mason+json",
           response=json.dumps(body)
        )
    #Let's skip "put" impelementation. We won't allow editing the move.
    #Instead in can be considered to delete the item first and then adding a new one.
    """
    def put(self, movement):
        if not request.json:
            return create_error_response(415, "Invalid content type", message=None)

        try:
            validate(request.json, Movement.get_schema())
            movement = Movement(
                timestamp_start=movement.timestamp_start,
                timestamp_end=movement.timestamp_end,
                move_number=movement.move_number,
                game_id=movement.game_id,
                author_id=movement.author_id,
                pawn_positions_before=movement.pawn_positions_before,
                pawn_positions_after=movement.pawn_positions_after
            )
            db.session.add(movement)
            db.session.commit()
            return Response(
                headers={
                    "Location": api.url_for(MovementItem, movement=movement.id)
                },
                status=201
            )
        
        except ValidationError as e:
            return create_error_response(400, "Invalid JSON document", str(e))
    """
    def delete(self, movement):
        if not Movement.query.filter(Movement.id == movement).first():
            return create_error_response(404, "Movement does not exist", message=None)
        movement = Movement.query.filter(Movement.id == movement).first()
        db.session.delete(movement)
        db.session.commit()
        return "", 204

"""
class MovementsbyPlayer(Resource):
    def init():
        return
"""
class MovementsinGame(Resource):
    def get(self, game):

        if not Game.query.filter(Game.id == game).first():
                return create_error_response(
                    400, 
                    "Invalid Request",
                    "No such game id."
                )

        body = MovementBuilder()
        body.add_namespace("brdgame", LINK_RELATIONS_URL)
        body.add_control(
            "brdgame:game",
            href=url_for("api.gameitem", game=game),
            method="GET",
            title="Get game associated to these moves"
        )
        body.add_control("self", url_for("api.movementsingame", game=game))
        body.add_control_movements_in_by(game=game)
        body.add_control_profile()
        body.add_control_up()
        body["items"] = []
        for movement in Movement.query.filter(Movement.game_id == game).all():
            item = MovementBuilder(
                id=movement.id,
                timestamp_start=str(movement.timestamp_start.isoformat() + "Z"),
                timestamp_end=str(movement.timestamp_end.isoformat() + "Z"),
                move_number=movement.move_number,
                game_id=movement.game_id,
                author_id=movement.author_id,
                pawn_positions_before=movement.pawn_positions_before,
                pawn_positions_after=movement.pawn_positions_after
            )
            item.add_control_game(movement.id)
            item.add_control_delete(movement.id)
            item.add_control_self(movement.id)
            item.add_control_up()
            item.add_control_profile()
            item.add_control_collection()
            #item.add_control_edit()
            item.add_control_author(movement.id)
            body["items"].append(item)

        

        return Response(
        status=200,
        content_type="application/vnd.mason+json",
        response=json.dumps(body)
        )
class MovementsinGamebyPlayer(Resource):
    def get(self, game, player):


        if not Game.query.filter(Game.id == game).first():
                return create_error_response(
                    400, 
                    "Invalid Request",
                    "No such game id."
                )

        if not Player.query.filter(Player.id == player).first():
                return create_error_response(
                    400, 
                    "Invalid Request",
                    "No such player id."
                )

        body = MovementBuilder()
        body.add_namespace("brdgame", LINK_RELATIONS_URL)
        body.add_control(
            "brdgame:game",
            href=url_for("api.gameitem", game=game),
            method="GET",
            title="Get game associated to these moves"
        )
        body.add_control("self", url_for("api.movementsingamebyplayer", game=game, player=player))
        body.add_control_profile()
        body.add_control(
            "author",
            href=url_for("api.playeritem", player=player),
            method="GET",
            title="Get author of the specified move(s)"
        )
        body.add_control(
            "up",
            href=url_for("api.movementsingame", game=game),
        )
        body["items"] = []
        for movement in (
            Movement.query.filter(Movement.game_id == game).filter(Movement.author_id == player).all()
        ):

            item = MovementBuilder(
                id=movement.id,
                timestamp_start=str(movement.timestamp_start.isoformat() + "Z"),
                timestamp_end=str(movement.timestamp_end.isoformat() + "Z"),
                move_number=movement.move_number,
                game_id=movement.game_id,
                author_id=movement.author_id,
                pawn_positions_before=movement.pawn_positions_before,
                pawn_positions_after=movement.pawn_positions_after
            )
            item.add_control_game(movement.id)
            item.add_control_delete(movement.id)
            item.add_control_self(movement.id)
            item.add_control_up()
            item.add_control_profile()
            item.add_control_collection()
            #item.add_control_edit()
            item.add_control_author(movement.id)
            body["items"].append(item)

        
        return Response(
           status=200,
           content_type="application/vnd.mason+json",
           response=json.dumps(body)
        )