import os
import json
from flask import Flask, url_for, Response
from flask_sqlalchemy import SQLAlchemy
from flask.cli import with_appcontext
from .constants import *

db = SQLAlchemy()

# Based on http://flask.pocoo.org/docs/1.0/tutorial/factory/#the-application-factory
# Modified to use Flask SQLAlchemy
def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
        SQLALCHEMY_DATABASE_URI="sqlite:///" + os.path.join(app.instance_path, "development.db"),
        SQLALCHEMY_TRACK_MODIFICATIONS=False
    )
    
    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_mapping(test_config)
        
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    
    db.init_app(app)

    from . import models
    from . import api
    app.cli.add_command(models.init_db_command)
    app.cli.add_command(models.generate_test_data)
    app.register_blueprint(api.api_bp)

    from . import utils

    @app.route(LINK_RELATIONS_URL)
    def send_link_relations():
        return "link relations"

    @app.route("/profiles/<profile>/")
    def send_profile(profile):
        return "you requests {} profile".format(profile)

    @app.route("/api/", methods=["GET"])
    def entry_point():
        body = utils.MasonBuilder()
        
        body.add_namespace(
            "brdgame",
            LINK_RELATIONS_URL
        )
        
        body.add_control(
            "brdgame:games-all",
            href=url_for("api.gamecollection"),
            method="GET",
            title="List all games"
        )
        body.add_control(
            "brdgame:players-all",
            href=url_for("api.playercollection"),
            method="GET",
            title="List all players"
        )
        body.add_control(
            "brdgame:movements-all",
            href=url_for("api.movementcollection"),
            method="GET",
            title="List all movements"
        )
        
        return Response(
            status=200,
            content_type="application/vnd.mason+json",
            response=json.dumps(body)
        )
        
    @app.route("/admin/")
    def admin_site():
        return app.send_static_file("html/admin.html")
        
        
    return app