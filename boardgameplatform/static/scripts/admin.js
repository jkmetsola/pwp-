const DEBUG = true;
const MASONJSON = "application/vnd.mason+json";
const PLAINJSON = "application/json";

/*
Modified from PWP course sensorhub admin template
*/

function renderError(jqxhr) {
    let msg = jqxhr.responseJSON["@error"]["@message"];
    $("div.notification").html("<p class='error'>" + msg + "</p>");
}

function renderMsg(msg) {
    $("div.notification").html("<p class='msg'>" + msg + "</p>");
}

function getResource(href, renderer) {
    $.ajax({
        url: href,
        success: renderer,
        error: renderError
    });
}

function sendData(href, method, item, postProcessor) {
    //console.log("Send data")
    //console.log(href)
    //console.log(JSON.stringify(item));
    //console.log(item)
    $.ajax({
        url: href,
        type: method,
        data: JSON.stringify(item),
        contentType: PLAINJSON,
        processData: false,
        success: postProcessor,
        error: renderError
    });
}

function sendDelete(href, method, postProcessor) {
    console.log("Send delete")
    console.log(href)
    console.log(method)
    $.ajax({
        url: href,
        type: 'delete',
        processData: false,
        success: postProcessor,
        error: renderError
    });
}

function followLink(event, a, renderer) {
    event.preventDefault();
    getResource($(a).attr("href"), renderer);
}

function followDeleteLink(event, a, renderer) {
    event.preventDefault();
    sendDelete($(a).attr("href"), 'delete', renderer)
    getResource("http://localhost:5000/api/movements/game/1/", renderMovementsinGame);
    location.reload();
}



//
// row v
//

function playerRow(item){
    let link = "<a href='" +
        "http://localhost:5000/api/movements/game/1/" +
        "' onClick='followLink(event, this, renderMovementsinGame)'>Front page</a>";
    
    let href_ = item["@controls"].self.href;
    
    console.log(href_);

    let delete_link = "<a href='" +
        item["@controls"].self.href +
        "' onClick='followDeleteLink(event, this, renderMovementsinGame)'>Delete player</a>";

    console.log("player row");

    return "<tr><td>" + item.id +
            "</td><td>" + item.name +
            "</td><td>" + item.surname +
            "</td><td>" + item.elo +
            "</td><td>" + link + 
            "</td><td>" + delete_link + 
            "</td></tr>";
}

function movementRow(item) {

    let link = "<a href='" +
        item["@controls"].author.href +
        "' onClick='followLink(event, this, renderPlayer)'>show player</a>";
    //console.log(item["@controls"].author.href);

    return "<tr><td>" + item.id +
            "</td><td>" + item.timestamp_start +
            "</td><td>" + item.timestamp_end +
            "</td><td>" + item.move_number +
            "</td><td>" + item.game_id +
            "</td><td>" + item.author_id +
            "</td><td>" + item.pawn_positions_before +
            "</td><td>" + item.pawn_positions_after +
            "</td><td>" + link + 
            "</td></tr>";
}

function appendMovementRow(body) {
    $(".resulttable tbody").append(movementRow(body));
}

function replacePlayerRow(body){
    let tbody = $(".resulttable tbody");
    tbody.empty();
    $(".resulttable tbody").append(playerRow(body));
}

//
// row ^
//


//
// submit & delete v
//

function getSubmittedMovement(data, status, jqxhr) {
    renderMsg("Successful");
    let href = jqxhr.getResponseHeader("Location");
    if (href) {
        getResource(href, appendMovementRow);
    }
}

function getSubmittedPlayer(data, status, jqxhr) {
    renderMsg("Successful");
    let href = jqxhr.getResponseHeader("Location");
    if (href) {
        getResource(href, replacePlayerRow);
    }
}

function submitMovement(event) {
    event.preventDefault();

    let data = {};
    let form = $("div.form form");
    
    data.game_id = parseInt($("input[name='game_id']").val());
    data.author_id = parseInt($("input[name='author_id']").val());
    data.pawn_positions_before = $("input[name='pawn_positions_before']").val();
    data.pawn_positions_after = $("input[name='pawn_positions_after']").val(); 
    data.timestamp_start = $("input[name='timestamp_start']").val();
    const date = new Date(Date.now());
    data.timestamp_end = date.toISOString().replace("Z", "000Z");
    sendData(form.attr("action"), form.attr("method"), data, getSubmittedMovement);
    location.reload()
}

function submitPlayerModification(event) {
    event.preventDefault();

    let data = {};
    let form = $("div.form form");
    
    data.name = $("input[name='name']").val();
    data.surname = $("input[name='surname']").val();
    data.elo = parseInt($("input[name='elo']").val());
    //console.log(form.attr("put player"))
    //console.log(form.attr("action"))
    sendData(form.attr("action"), form.attr("method"), data, getSubmittedPlayer);
    location.reload();
}

/*
function deletePlayer(event) {

    event.preventDefault();
    let form = $("div.form form");
    //console.log(form.attr("delete player"))
    //console.log(form.attr("action"))
    sendDelete(form.attr("action"), "DELETE", "Player deleted succesfully");
    //location.reload();
}
*/


//
// submit & delete ^
//


//
// render form v
//

function renderPlayersForm(ctrl) {

    //console.log(ctrl)
    //console.log(ctrl.href)

    let form = $("<form>");
    let name = ctrl.schema.properties.name
    let surname = ctrl.schema.properties.surname
    let elo = ctrl.schema.properties.elo;
    
    form.attr("action", ctrl.href);
    form.attr("method", ctrl.method);
    form.submit(submitPlayerModification);
    //form.submit(deletePlayer);
    form.append("<label>" + name.description + "</label>");
    form.append("<input type='text' name='name'>");
    form.append("<label>" + surname.description + "</label>");
    form.append("<input type='text' name='surname'>");
    form.append("<label>" + elo.description + "</label>");
    form.append("<input type='number' name='elo'>");


    ctrl.schema.required.forEach(function (property) {
        $("input[name='" + property + "']").attr("required", true);
    });
    form.append("<input type='submit' name='submit' value='Replace player'>");
    $("div.form").html(form);
}


function renderMovementForm(ctrl, item) {
    let form = $("<form>");
    let game_id = ctrl.schema.properties.game_id
    let author_id = ctrl.schema.properties.author_id
    let pawn_positions_before = ctrl.schema.properties.pawn_positions_before;
    let pawn_positions_after = ctrl.schema.properties.pawn_positions_after;
    let timestamp_start = ctrl.schema.properties.timestamp_start;


    let pawn_positions_value_last_turn = "";

    let timestamp_value_last_turn = "";
    const date = new Date(Date.now());
    timestamp_value_last_turn = date.toISOString().replace("Z", "000Z");

    if (typeof item !== "undefined"){

        if (typeof item["pawn_positions_after"] !== "undefined") {
            pawn_positions_value_last_turn = JSON.stringify(item["pawn_positions_after"]);
        }
        if (typeof item["timestamp_end"] !== "undefined"){
            timestamp_value_last_turn = JSON.stringify(item["timestamp_end"])
        }
    }
    
    
    
    form.attr("action", ctrl.href);
    form.attr("method", ctrl.method);
    form.submit(submitMovement);
    form.append("<label>" + game_id.description + "</label>");
    form.append("<input type='number' input value=1 name='game_id'>");
    form.append("<label>" + author_id.description + "</label>");
    form.append("<input type='number' input value=1 name='author_id'>");
    form.append("<label>" + pawn_positions_before.description + "</label>");
    form.append("<input type='text' input value=" + pawn_positions_value_last_turn + " name='pawn_positions_before'>"); //take pawn positions before from previous move
    form.append("<label>" + pawn_positions_after.description + "</label>");
    form.append("<input type='text' name='pawn_positions_after'>");
    form.append("<label>" + timestamp_start.description + "</label>");
    form.append("<input type='text' input value=" + timestamp_value_last_turn + " name='timestamp_start'>"); // timestamp_start=previous turn timestamp_end

    ctrl.schema.required.forEach(function (property) {
        $("input[name='" + property + "']").attr("required", true);
    });
    form.append("<input type='submit' name='submit' value='Submit'>");
    $("div.form").html(form);
}
//
// render form ^
//



//
// render v
//
function renderPlayer(body){

    $("div.navigation").empty();
    $(".resulttable thead").html(
        "<tr><th>Id" + 
        "</th><th>Name" +
        "</th><th>Surname" +
        "</th><th>elo" +
        "</th></tr>"
    );
    replacePlayerRow(body)
    //$.getJSON("http://localhost:5000/api/players/", function(data) {
    renderPlayersForm(body["@controls"]["edit"]); //take last element of the array
    //});
}

function renderMovementsinGame(body) {
    $("div.navigation").empty();
    $(".resulttable thead").html(
        "<tr><th>Move Id" + 
        "</th><th>Timestamp start" +
        "</th><th>Timestamp end" +
        "</th><th>Move number" +
        "</th><th>Game_id" +
        "</th><th>Author_id" +
        "</th><th>Pawn positions before" +
        "</th><th>Pawn positions after" +
        "</th></tr>"
    );
    let tbody = $(".resulttable tbody");
    tbody.empty();
    body.items.forEach(function (item) {
        tbody.append(movementRow(item));
    });
    //console.log(body.items.slice(-1)[0]);
    $.getJSON("http://localhost:5000/api/movements/", function(data) {
        renderMovementForm(data["@controls"]["brdgame:add-movement"], body.items.slice(-1)[0]); //take last element of the array
    });
    
}

//
// render ^
//

$(document).ready(function () {
    getResource("http://localhost:5000/api/movements/game/1/", renderMovementsinGame);
});

/*
function renderPlayers(body){
    $("div.navigation").empty();
    $(".resulttable thead").html(
        "<tr><th>Id" + 
        "</th><th>Name" +
        "</th><th>Surname" +
        "</th><th>elo" +
        "</th></tr>"
    );
    let tbody = $(".resulttable tbody");
    tbody.empty();
    body.items.forEach(function (item) {
        tbody.append(movementRow(item));
    });
    //console.log(body.items.slice(-1)[0]);
    $.getJSON("http://localhost:5000/api/players/", function(data) {
        renderMovementForm(data["@controls"]["brdgame:add-player"], body.items.slice(-1)[0]); //take last element of the array
    });
}
*/