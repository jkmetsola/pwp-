import os
import pytest
import tempfile
import requests
import json
import string
import random
import sys
from pprint import PrettyPrinter
from datetime import datetime, timedelta, timezone
from jsonschema import validate
from sqlalchemy.engine import Engine
from sqlalchemy import event
from sqlalchemy.exc import IntegrityError, StatementError

sys.path.append('../')

from boardgameplatform import create_app, db
from boardgameplatform.constants import *
from boardgameplatform.models import Game, Player, Movement

pp = PrettyPrinter(sort_dicts=False)

@pytest.fixture
def client():
    db_fd, db_fname = tempfile.mkstemp()
    config = {
        "SQLALCHEMY_DATABASE_URI": "sqlite:///" + db_fname,
        "TESTING": True
    }
    print(db_fd)
    print(db_fname)
    app = create_app(config)
    with app.app_context():
        db.create_all()
        _populate_db()

    #print(app.test_client.__doc__)

    yield app.test_client()
    os.close(db_fd)
    os.unlink(db_fname)

class TestMovement(object):

    RESOURCE_URL = "/api/movements/"

    def test_create_movement_and_get(self, client):
        body = _json_movement(_game_id=1, _author_id=1)
        resp = client.post(self.RESOURCE_URL, json=body)
        print(json.dumps(resp.get_json()))
        assert resp.status_code == 201
        location_header = False
        location_header = _extract_location_header(resp)
        assert location_header
        #print(location_header)

        print("GET REQ")
        print(location_header)

        resp = client.get(str(location_header))
        print(json.dumps(resp.get_json()))

        assert resp.get_json()
        assert resp.get_json()["@controls"]
        assert resp.get_json()["@controls"]["self"]["href"] == location_header.split("http://localhost")[1]
        assert resp.status_code == 200

    def test_delete_move(self, client):
        resp = client.delete("/api/movements/1/")
        assert resp.status_code == 204
        #print(json.dumps(resp.get_json()))

    def test_delete_non_existing_move(self, client):
        resp = client.delete("/api/movements/2435353646/")
        assert resp.status_code == 404
        print(json.dumps(resp.get_json()))

class TestMovementCollection(object):

    RESOURCE_URL = "/api/movements/"

    def test_get(self, client):
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200
        body = json.loads(resp.data)
        print(json.dumps(resp.get_json()))

    def test_post(self, client):

        body = _json_movement(_game_id=1, _author_id=1)
        resp = client.post(self.RESOURCE_URL, json=body)
        print(resp.__dict__)
        assert resp.status_code == 201
        location_header = False
        location_header = _extract_location_header(resp)
        assert location_header

    def test_post_get_proper_chess_move(self, client):
        #create new game ...
        body = _json_game()
        resp = client.post("/api/games/", json=body)
        assert resp.status_code == 201
        location = _extract_location_header(resp)
        game_id = client.get(location).get_json()["id"]
        #...and make first move to that game
        body = _json_proper_chess_movement1(_game_id=game_id, _author_id=1)
        print(json.dumps(body))
        resp = client.post(self.RESOURCE_URL, json=body)
        assert resp.status_code == 201
        #.. and get the movement item just created
        location = _extract_location_header(resp)
        resp = client.get(location)
        assert resp.status_code == 200
        print(json.dumps(resp.get_json()))

    def test_post_with_inconsistent_timestamp(self,client):
        body = _json_movement_invalid_times(_game_id=1, _author_id=1)
        resp = client.post(self.RESOURCE_URL, json=body)
        #print(resp.__dict__)
        assert resp.status_code == 400
        print(json.dumps(resp.get_json()))

    def test_post_same_move_positions_before_and_after(self,client):
        body = _json_movement_same_before_after(_game_id=1, _author_id=1)
        resp = client.post(self.RESOURCE_URL, json=body)
        #print(resp.__dict__)
        assert resp.status_code == 400
        print(json.dumps(resp.get_json()))

    def test_post_invalid_game_id(self, client):

        body = _json_movement(_game_id=100023, _author_id=1)
        resp = client.post(self.RESOURCE_URL, json=body)
        #print(resp.__dict__)
        assert resp.status_code == 400
        print(json.dumps(resp.get_json()))

    def test_post_invalid_player_id(self, client):

        body = _json_movement(_game_id=1, _author_id=435636)
        resp = client.post(self.RESOURCE_URL, json=body)
        #print(resp.__dict__)
        assert resp.status_code == 400
        print(json.dumps(resp.get_json()))


    def test_3_post_and_get(self, client):
        body = _json_movement(_game_id=1, _author_id=1)
        resp = client.post(self.RESOURCE_URL, json=body)
        assert resp.status_code == 201
        body = _json_movement(_game_id=2, _author_id=1)
        resp = client.post(self.RESOURCE_URL, json=body)
        assert resp.status_code == 201
        body = _json_movement(_game_id=3, _author_id=2)
        resp = client.post(self.RESOURCE_URL, json=body)
        assert resp.status_code == 201

        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200
        body = json.loads(resp.data)

        return

class TestMovementsinGame(object):

    RESOURCE_URL = "/api/movements/game/3/"

    def test_get(self, client):
        resp = client.get(self.RESOURCE_URL)
        print(json.dumps(resp.get_json()))
        assert resp.status_code == 200
        body = json.loads(resp.data)
        return

    def test_get_with_invalid_game_id(self, client):
        resp = client.get("/api/movements/game/77554/")
        print(json.dumps(resp.get_json()))
        assert resp.status_code == 400
        body = json.loads(resp.data)
        return

class TestMovementsinGamebyPlayer(object):
    
    RESOURCE_URL = "/api/movements/game/3/player/3/"

    def test_get(self, client):
        resp = client.get(self.RESOURCE_URL)
        print(json.dumps(resp.get_json()))
        assert resp.status_code == 200
        body = json.loads(resp.data)
        return

    def test_get_with_invalid_game_id(self, client):
        resp = client.get("/api/movements/game/64/player/3/")
        print(json.dumps(resp.get_json()))
        assert resp.status_code == 400
        body = json.loads(resp.data)
        return

    def test_get_with_invalid_player_id(self, client):
        resp = client.get("/api/movements/game/3/player/653/")
        print(json.dumps(resp.get_json()))
        assert resp.status_code == 400
        body = json.loads(resp.data)
        return

class TestMovementProfile(object):

    RESOURCE_URL = MOVEMENT_PROFILE

    def test_get(self, client):
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200

def _populate_db():
    player1 = Player(        
        name="tomato",
        surname="cucumber",
        elo=69
    )
    game1 = Game()
    db.session.add_all([player1,game1])
    db.session.commit()
    movement1 = Movement(
        timestamp_start=datetime.utcnow(),
        timestamp_end=datetime.utcnow()+timedelta(seconds=60),
        move_number=1,
        game_id=game1.id,
        author_id=player1.id,
        pawn_positions_before="jeje",
        pawn_positions_after="wowo"
    )   
    db.session.add(movement1)
    db.session.commit()
    player2 = _object_player()
    player3 = _object_player()
    game2 = Game()
    game3 = Game()
    db.session.add_all([player2, player3, game2, game3])
    db.session.commit()
    movement2 = _object_movement(_author_id=player2.id, _game_id=game2.id)
    db.session.add(movement2)
    db.session.commit()
    movement3 = _object_movement(_author_id=player3.id, _game_id=game2.id)
    db.session.add(movement3)
    db.session.commit()
    movement4 = _object_movement(_author_id=player2.id, _game_id=game3.id)
    db.session.add(movement4)
    db.session.commit()
    movement5 = _object_movement(_author_id=player3.id, _game_id=game3.id)
    db.session.add(movement5)
    db.session.commit()
    movement6 = _object_movement(_author_id=player2.id, _game_id=game3.id)
    db.session.add(movement6)
    db.session.commit()
    movement7 = _object_movement(_author_id=player3.id, _game_id=game3.id)
    db.session.add(movement7)
    db.session.commit()

def _json_player():
    name = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=10))
    surname = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=10))
    elo = int(10*random.random())
    player = {
        "name": name,
        "surname": surname,
        "elo": elo
    }
    return player

def _json_movement(_game_id, _author_id):
    clock = datetime.now(timezone.utc)
    timestamp_start=clock.isoformat().replace("+00:00", "Z")
    timestamp_end=(clock+timedelta(seconds=60)).isoformat().replace("+00:00", "Z")
    #move_number=1+len(Movement.query.filter(Movement.game_id == _game_id).all())
    game_id=_game_id
    author_id=_author_id
    pawn_positions_before="jeje"
    pawn_positions_after="wowo"

    movement = {
        "timestamp_start": str(timestamp_start),
        "timestamp_end": str(timestamp_end),
        #"move_number": int(move_number),
        "game_id": int(game_id),
        "author_id": int(author_id),
        "pawn_positions_before": str(pawn_positions_before),
        "pawn_positions_after": str(pawn_positions_after)
    }
    return movement

def _json_movement_same_before_after(_game_id, _author_id):
    clock = datetime.now(timezone.utc)
    timestamp_start = clock.isoformat().replace("+00:00", "Z")
    timestamp_end = (clock+timedelta(seconds=60)).isoformat().replace("+00:00", "Z")
    #move_number=1+len(Movement.query.filter(Movement.game_id == _game_id).all())
    game_id = _game_id
    author_id = _author_id
    pawn_positions_before = _chess_positions_example_1st_move()
    pawn_positions_after = _chess_positions_example_1st_move()

    movement = {
        "timestamp_start": str(timestamp_start),
        "timestamp_end": str(timestamp_end),
        #"move_number": int(move_number),
        "game_id": int(game_id),
        "author_id": int(author_id),
        "pawn_positions_before": str(pawn_positions_before),
        "pawn_positions_after": str(pawn_positions_after)
    }
    return movement

def _json_movement_invalid_times(_game_id, _author_id):
    clock = datetime.now(timezone.utc)
    timestamp_start=clock.isoformat().replace("+00:00", "Z")
    timestamp_end=(clock-timedelta(seconds=60)).isoformat().replace("+00:00", "Z")
    #move_number=1+len(Movement.query.filter(Movement.game_id == _game_id).all())
    game_id=_game_id
    author_id=_author_id
    pawn_positions_before="jeje"
    pawn_positions_after="wowo"

    movement = {
        "timestamp_start": str(timestamp_start),
        "timestamp_end": str(timestamp_end),
        #"move_number": int(move_number),
        "game_id": int(game_id),
        "author_id": int(author_id),
        "pawn_positions_before": str(pawn_positions_before),
        "pawn_positions_after": str(pawn_positions_after)
    }
    return movement

def _json_proper_chess_movement1(_game_id, _author_id):
    clock = datetime.now(timezone.utc)
    timestamp_start=clock.isoformat().replace("+00:00", "Z")
    timestamp_end=(clock+timedelta(seconds=60)).isoformat().replace("+00:00", "Z")
    #move_number=1+len(Movement.query.filter(Movement.game_id == _game_id).all())
    game_id=_game_id
    author_id=_author_id
    pawn_positions_before=_chess_positions_start()
    pawn_positions_after=_chess_positions_example_1st_move()

    movement = {
        "timestamp_start": str(timestamp_start),
        "timestamp_end": str(timestamp_end),
        #"move_number": int(move_number),
        "game_id": int(game_id),
        "author_id": int(author_id),
        "pawn_positions_before": str(pawn_positions_before),
        "pawn_positions_after": str(pawn_positions_after)
    }
    return movement

def _json_game():
    return {}

def _object_player():
    _name = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=10))
    _surname = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=10))
    _elo = int(random.randint(1,2800))
    player1 = Player(        
        name=_name,
        surname=_surname,
        elo=_elo
    )
    return player1

def _object_movement(_author_id, _game_id, _pawn_positions_before="dummy1", _pawn_positions_after="dummy2"):
    timestamp_start=datetime.now(timezone.utc)
    movement=Movement(
        timestamp_start=timestamp_start,
        timestamp_end=timestamp_start+timedelta(seconds=random.randint(1,200)),
        move_number=1+len(Movement.query.filter(Movement.game_id == _game_id).all()),
        game_id=_game_id,
        author_id=_author_id,
        pawn_positions_before=_pawn_positions_before,
        pawn_positions_after=_pawn_positions_after
    )
    return movement



def _extract_location_header(resp):
    for item in resp.headers:
        if item[0]=="Location":
            location_header = item[1]
    return location_header

def _chess_positions_start():
    rows = []
    rows.append("bR,bN,bB,bQ,bK,bB,bN,bR") #row1
    rows.append("bS,bS,bS,bS,bS,bS,bS,bS") #row2
    rows.append(",,,,,,,")                 #row3
    rows.append(",,,,,,,")                 #row4
    rows.append(",,,,,,,")                 #row5
    rows.append(",,,,,,,")                 #row6
    rows.append("wS,wS,wS,wS,wS,wS,wS,wS") #row7
    rows.append("wR,wN,wB,wQ,wK,wB,wN,wR") #row8

    csv = ""
    for row in rows:
        csv = csv + row + "\n"
    return csv

def _chess_positions_example_1st_move():  
    rows = []
    rows.append("bR,bN,bB,bQ,bK,bB,bN,bR") #row1
    rows.append("bS,bS,bS,bS,bS,bS,bS,bS") #row2
    rows.append(",,,,,,,")                 #row3
    rows.append(",,,,,,,")                 #row4
    rows.append(",,wS,,,,,")                 #row5
    rows.append(",,,,,,,")                 #row6
    rows.append("wS,wS,,wS,wS,wS,wS,wS") #row7
    rows.append("wR,wN,wB,wQ,wK,wB,wN,wR") #row8

    csv = ""
    for row in rows:
        csv = csv + row + "\n"
    return csv

def _chess_positions_example_2nd_move():  
    rows = []
    rows.append("bR,bN,bB,bQ,bK,bB,bN,bR") #row1
    rows.append("bS,bS,,bS,bS,bS,bS,bS") #row2
    rows.append(",,,,,,,")                 #row3
    rows.append(",,bS,,,,,")                 #row4
    rows.append(",,wS,,,,,")                 #row5
    rows.append(",,,,,,,")                 #row6
    rows.append("wS,,wS,wS,wS,wS,wS,wS") #row7
    rows.append("wR,wN,wB,wQ,wK,wB,wN,wR") #row8

    csv = ""
    for row in rows:
        csv = csv + row + "\n"
    return csv

