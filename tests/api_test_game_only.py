import os
import pytest
import tempfile
import requests
import json
import string
import random
import sys
from pprint import PrettyPrinter
from datetime import datetime, timedelta, timezone
from jsonschema import validate
from sqlalchemy.engine import Engine
from sqlalchemy import event
from sqlalchemy.exc import IntegrityError, StatementError

sys.path.append('../')

from boardgameplatform import create_app, db
from boardgameplatform.constants import *
from boardgameplatform.models import Game, Player, Movement

pp = PrettyPrinter(sort_dicts=False)

@pytest.fixture
def client():
    db_fd, db_fname = tempfile.mkstemp()
    config = {
        "SQLALCHEMY_DATABASE_URI": "sqlite:///" + db_fname,
        "TESTING": True
    }
    print(db_fd)
    print(db_fname)
    app = create_app(config)
    with app.app_context():
        db.create_all()
        _populate_db()

    #print(app.test_client.__doc__)

    yield app.test_client()
    os.close(db_fd)
    os.unlink(db_fname)

class TestSingleGame(object):

    RESOURCE_URL = "/api/games/"
    
    def test_create_game_and_get(self, client):

        body = _json_game()
        print(body)
        resp = client.post(self.RESOURCE_URL, json=body)

        print(resp.get_json())
        assert resp.status_code == 201
        location_header_present = False
        for item in resp.headers:
            if item[0]=="Location":
                location_header = item[1]
                location_header_present = True
        assert location_header
        print(location_header)
        resp = client.get(str(location_header))

        print(json.dumps(resp.get_json()))

        assert resp.get_json()
        assert resp.get_json()["@controls"]
        assert resp.status_code == 200

    def test_get_game_with_invalid_id(self, client):

        resp = client.get("/api/games/69/")
        assert resp.status_code == 404
        print(json.dumps(resp.get_json()))

    def test_post_game_with_invalid_json(self, client):
        body = _invalid_json_game()
        resp = client.post(self.RESOURCE_URL, json=body)
        assert resp.status_code == 415
        print(json.dumps(resp.get_json()))
        print((resp.headers))

    def test_delete_game(self, client):
        
        resp = client.get("/api/movements/1/")
        assert resp.status_code == 200

        resp = client.delete("/api/games/1/")
        assert resp.status_code == 204
        resp = client.get("/api/games/1/")
        assert resp.status_code == 404

        resp = client.get("/api/movements/1/")
        assert resp.status_code == 404

        #print(json.dumps(resp.get_json()))

    def test_delete_nonexistent_game(self, client):
        resp = client.delete("/api/games/4366/")
        print(json.dumps(resp.get_json()))
        assert resp.status_code == 404

class TestGameCollection(object):

    RESOURCE_URL = "/api/games/"

    def test_get(self, client):
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200
        print(json.dumps(resp.get_json()))

    def test_post(self, client):
        body = _json_game()
        resp = client.post(self.RESOURCE_URL, json=body)
        print(resp.__dict__)
        assert resp.status_code == 201
        location_header = False
        for item in resp.headers:
            if item[0]=="Location":
                location_header = True
        assert location_header

    def test_3_post_and_get(self, client):
        body = _json_game()
        resp = client.post(self.RESOURCE_URL, json=body)
        assert resp.status_code == 201
        body = _json_game()
        resp = client.post(self.RESOURCE_URL, json=body)
        assert resp.status_code == 201
        body = _json_game()
        resp = client.post(self.RESOURCE_URL, json=body)
        assert resp.status_code == 201

        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200
        body = json.loads(resp.data)   

class TestGameProfile(object):

    RESOURCE_URL = GAME_PROFILE

    def test_get(self, client):
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200

def _populate_db():
    player1 = Player(        
        name="tomato",
        surname="cucumber",
        elo=69
    )
    game1 = Game()
    db.session.add_all([player1,game1])
    db.session.commit()
    movement1 = Movement(
        timestamp_start=datetime.utcnow(),
        timestamp_end=datetime.utcnow()+timedelta(seconds=60),
        move_number=1,
        game_id=game1.id,
        author_id=player1.id,
        pawn_positions_before="jeje",
        pawn_positions_after="wowo"
    )   
    db.session.add(movement1)
    db.session.commit()
    player2 = _object_player()
    player3 = _object_player()
    game2 = Game()
    game3 = Game()
    db.session.add_all([player2, player3, game2, game3])
    db.session.commit()
    movement2 = _object_movement(_author_id=player2.id, _game_id=game2.id)
    db.session.add(movement2)
    db.session.commit()
    movement3 = _object_movement(_author_id=player3.id, _game_id=game2.id)
    db.session.add(movement3)
    db.session.commit()
    movement4 = _object_movement(_author_id=player2.id, _game_id=game3.id)
    db.session.add(movement4)
    db.session.commit()
    movement5 = _object_movement(_author_id=player3.id, _game_id=game3.id)
    db.session.add(movement5)
    db.session.commit()
    movement6 = _object_movement(_author_id=player2.id, _game_id=game3.id)
    db.session.add(movement6)
    db.session.commit()
    movement7 = _object_movement(_author_id=player3.id, _game_id=game3.id)
    db.session.add(movement7)
    db.session.commit()

def _json_game():
    return {}

def _object_player():
    _name = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=10))
    _surname = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=10))
    _elo = int(random.randint(1,2800))
    player1 = Player(        
        name=_name,
        surname=_surname,
        elo=_elo
    )
    return player1

def _object_movement(_author_id, _game_id):
    timestamp_start=datetime.now(timezone.utc)
    movement=Movement(
        timestamp_start=timestamp_start,
        timestamp_end=timestamp_start+timedelta(seconds=random.randint(1,200)),
        move_number=1+len(Movement.query.filter(Movement.game_id == _game_id).all()),
        game_id=_game_id,
        author_id=_author_id,
        pawn_positions_before="jeje",
        pawn_positions_after="wowo"
    )
    return movement

def _extract_location_header(resp):
    for item in resp.headers:
        if item[0]=="Location":
            location_header = item[1]
    return location_header


def _invalid_json_game():
    return {"name": "sdkasjdl"}

def _movements_by_game_id(_game_id):
    print(Movement.query.filter(Movement.game_id == _game_id).first())
    return Movement.query.filter(Movement.game_id == _game_id).first()





