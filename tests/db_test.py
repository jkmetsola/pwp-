import os
import pytest
import tempfile
import requests
import json
import string
import random
import sys
from pprint import pprint
from datetime import datetime, timedelta
from time import sleep

from sqlalchemy.engine import Engine
from sqlalchemy import event
from sqlalchemy.exc import IntegrityError, StatementError

sys.path.append('../')

from boardgameplatform import create_app, db
from boardgameplatform.models import Game, Player, Movement

@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()

@pytest.fixture
def app():
    db_fd, db_fname = tempfile.mkstemp()
    config = {
        "SQLALCHEMY_DATABASE_URI": "sqlite:///" + db_fname,
        "TESTING": True
    }

    print(db_fd)
    print(db_fname)
    
    app = create_app(config)
    
    with app.app_context():
        db.create_all()
        
    yield app
    os.close(db_fd)
    os.unlink(db_fname)

def test_creating_game(app):

    with app.app_context():
        game1 = Game() 
        db.session.add(game1)
        db.session.commit()
        assert Game.query.count() == 1

def test_creating_player(app):
    with app.app_context():
        player1 = Player(
            name = "dummy",
            surname = "yummu",
            elo = 1300
        )
        db.session.add(player1)
        db.session.commit()
        assert Player.query.count() == 1
        assert Player.query.first().name == "dummy"

def test_creating_movement(app):
    with app.app_context():
        player1 = _create_player(Player)
        game1 = Game()
        db.session.add(player1)
        db.session.add(game1)
        db.session.commit()
        movement1 = Movement(
            timestamp_start=datetime.now(),
            timestamp_end=datetime.now()+timedelta(seconds=60),
            move_number=1,
            game_id=game1.id,
            author_id=player1.id,
            pawn_positions_before="jeje",
            pawn_positions_after="wowo"
        )
        db.session.add(movement1)
        db.session.commit()
        assert Movement.query.count() == 1
        assert Movement.query.first().pawn_positions_before == "jeje"

def test_invalid_datetimes_on_movement(app):
    with app.app_context():
        player1 = _create_player(Player)
        game1 = Game()
        db.session.add(player1)
        db.session.add(game1)
        db.session.commit()
        movement1 = Movement(
            timestamp_start="0202-2334-21",
            timestamp_end="0202-2334-2s451",
            move_number=1,
            game_id=game1.id,
            author_id=player1.id,
            pawn_positions_before="jeje",
            pawn_positions_after="wowo"
        )
        db.session.add(movement1)
        with pytest.raises(StatementError):
            db.session.commit()

        db.session.rollback()

        assert Movement.query.count() == 0
        #assert Movement.query.first().pawn_positions_before == "jeje"

def test_invalid_players(app):
    with app.app_context():
        player1 = _create_player(Player)
        player2 = _create_player(Player)
        player3 = _create_player(Player)

        db.session.add_all([player1, player2, player3])
        db.session.commit()

        player1.elo = "asdasg"
        with pytest.raises(StatementError):
            db.session.commit()
        db.session.rollback()

        #player2.elo = "125"
        #with pytest.raises(StatementError):
        #    db.session.commit()
        #db.session.rollback()

        player3.elo = 5.6546
        with pytest.raises(StatementError):
            db.session.commit()
        db.session.rollback()

        player4 = Player(
            name = "sadfbhdfh",
            surname = "dfhdfh",
            elo = "asdghy4"
        )
        db.session.add(player4)
        with pytest.raises(StatementError):
            db.session.commit()

        db.session.rollback()

        #assert Player.query.count() == 0
        #seems to be impossible to restrict input string to be of type "digit"...
        #We must catch incorrect string before API tries to add to db.

def _create_player(Player):
    name = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=10))
    surname = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=10))
    elo = int(10*random.random())
    player1 = Player(
            name=name,
            surname=surname,
            elo=elo
    )
    return player1

def test_creating_not_unique_movement_number_to_same_game(app):
    with app.app_context():
        player1 = _create_player(Player)
        game1 = Game()
        db.session.add(player1)
        db.session.add(game1)
        db.session.commit()
        movement1 = Movement(
            timestamp_start=datetime.now(),
            timestamp_end=datetime.now()+timedelta(seconds=60),
            move_number=1,
            game_id=game1.id,
            author_id=player1.id,
            pawn_positions_before="jeje",
            pawn_positions_after="wowo"
        )
        db.session.add(movement1)
        db.session.commit()
        assert Movement.query.count() == 1
        assert Movement.query.first().pawn_positions_before == "jeje"
        movement2 = Movement(
            timestamp_start=datetime.now(),
            timestamp_end=datetime.now()+timedelta(seconds=60),
            move_number=1,
            game_id=game1.id,
            author_id=player1.id,
            pawn_positions_before="jeje",
            pawn_positions_after="wowo"
        )
        db.session.add(movement2)
        with pytest.raises(IntegrityError):
            db.session.commit()

def test_creating_not_unique_movement_number_to_different_game(app):
    with app.app_context():
        player1 = _create_player(Player)
        game1 = Game()
        game2 = Game()
        db.session.add_all([player1,game1,game2])
        db.session.commit()
        movement1 = Movement(
            timestamp_start=datetime.now(),
            timestamp_end=datetime.now()+timedelta(seconds=60),
            move_number=1,
            game_id=game1.id,
            author_id=player1.id,
            pawn_positions_before="jeje",
            pawn_positions_after="wowo"
        )
        db.session.add(movement1)
        db.session.commit()
        assert Movement.query.count() == 1
        assert Movement.query.first().pawn_positions_before == "jeje"
        movement2 = Movement(
            timestamp_start=datetime.now(),
            timestamp_end=datetime.now()+timedelta(seconds=60),
            move_number=1,
            game_id=game2.id,
            author_id=player1.id,
            pawn_positions_before="jeje",
            pawn_positions_after="wowo"
        )
        db.session.add(movement2)
        db.session.commit()



"""
def _populate_db():
    
    product = Product(        
        handle="tomato",
        weight=4.56,
        price=25.6
    )
    app.db.session.add(product)
    app.db.session.commit()
"""   






