# PWP SPRING 2021
# PROJECT NAME
# Group information
* Student 1. Jaakko Metsola jkmetsola@hotmail.fi

Full documentation of the API: https://chessapi3.docs.apiary.io/#


Before starting:

	1. Dependencies
			Use "requirements.txt" file to install needed packages.

				pip install [options] -r <requirements file> [package-index-options] ...

General usage:


	1. How to initialise and run the server

			1.1 "set FLASK_ENV=development"
			1.2 "set FLASK_APP=boardgameplatform"
			1.3 cd to project root directory
			1.4 "flask init-db"
			1.5 "flask run"
			
			
	2. How to populate and setup the database.

			2.1. Database is already initialised after step 1.
			2.2. To populate the db "manually", without api, you can execute following command (from project root folder)
			
				2.2.1 "flask testgen"
			
		
		
How to execute tests:

	1. cd to "tests" folder
	2. "pytest api_test_game_only.py api_test_movement_only.py api_test_player_only.py db_test.py --cov"
	3. You can also run one test at a time.
	4. If you want to see the prints for the succesful tests, use "-r A" flag
	
	### note there's one errorous test with db. For some reason it seems hard to create such constraint to db that it accepts only "digit" values.
	### Validation is done in the API.
	
How to use alpha client:

	1. Initialise db as instructed in "General usage". (possbily delete existing .db file from instance folder)
	2. Open browser and go to "http://localhost:5000/admin/"
	3. This is quite restricted client, there's harcoded view of "http://localhost:5000/api/movements/game/1/"
		3.1. It's possible to POST movement from this view.
		3.2. There's bad validation for pawn layout because we wanted to have general usage possible.
