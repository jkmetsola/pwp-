from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
import random

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///testidatabase.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)


class player(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String, nullable=False)
    Surname = db.Column(db.String, nullable=False)
    Elo = db.Column(db.Integer, nullable=False)
    api_key = db.Column(db.String, nullable=False)

class game(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    player_host = db.Column(db.Integer, db.ForeignKey("player.id"))
    player_guest = db.Column(db.Integer, db.ForeignKey("player.id"))
    turn = db.Column(db.Integer, nullable=True)
    

class piece(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.Integer, db.ForeignKey("piece_type.id"))
    player_id = db.Column(db.Integer, db.ForeignKey("player.id"))
    active = db.Column(db.Integer, nullable=False)

    
class position(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    piece_id = db.Column(db.Integer, db.ForeignKey("piece.id"))
    position_x = db.Column(db.String, nullable=False)
    position_y = db.Column(db.Integer, nullable=False)
    
   
class piece_type(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    piece_name = db.Column(db.String, nullable=False)


#test function to test that your database stores wanted data.
def testdb():
    #Create a couple of test players to the database
    player1 = player(Name='testikaveri', Surname='testinsuku', Elo=1000, api_key=str(random.randint(3, 1000)))
    player2 = player(Name='testituttu', Surname='testisuku2', Elo=1400, api_key=str(random.randint(3, 1000)))
    player3 = player(Name='testitesti', Surname='testisuku3', Elo=1650, api_key=str(random.randint(3, 1000)))
    player4 = player(Name='testivielä', Surname='testisuku4', Elo=1850, api_key=str(random.randint(3, 1000)))
    db.session.add(player1)
    db.session.add(player2)
    db.session.add(player3)
    db.session.add(player4)
    
    game_turn = game(turn=1)
    db.session.add(game_turn)
    
    piece_active = piece(active=0)
    db.session.add(piece_active)
    
    piece_current_xy = position(position_x = "A", position_y = 2)
    db.session.add(piece_current_xy)
    
    piece_in_action = piece_type(piece_name = "wk1")
    db.session.add(piece_in_action)
    
    db.session.commit()

db.create_all()
testdb()