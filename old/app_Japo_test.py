from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError
from sqlalchemy.engine import Engine
from sqlalchemy import event

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///test.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

#template test(not ready yet):

#correct player:
player2 = Player(name=546, surname="35k4", elo="6") #it is up to implementation whether all values are accepted.

#illegal:

#elo is illegal
player1 = Player(name=546, surname="35k4", elo=4.6) #it is up to implementation whether all values are accepted.

player3 = Player(elo=4) #this is illegal because name and surname are non-nullable
player4 = Player(name="556") #his is also illegal
player4 = Player(surname="556") #his is also illegal

player5 = Player(elo=4, name="556") #his is also illegal
player6 = Player(elo=4, surname="556") #his is also illegal
player7 = Player(surname="556", name="6788") #his is also illegal


#legal:
game1 = Game(player_host=player1, player_guest=player2) #create game with both players
game2 = Game(player_host=player1) #create game with only host (lobby leader)


#illegal:
game3 = Game(player_guest=player2) # this is illegal because 
game4 = Game(player_host=player1, player_guest=player2) #players cannot be same

#first test correct movement
movement1 = Movement(
    #game_id should be extracted from game that will be defined
    #following mapping used: 
    #A8,B8,C8,D8,E8,F8,G8,H8 \n A7,B7,C7,D7,E7,F7,G7,H7 \n A6,B6,C6,D6,E6,F6,G6,H6 \n A5,B5,C5,D5,E5,F5,G5,H5 \n A4,B4,C4,D4,E4,F4,G4,H4 \n A3,B3,C3,D3,E3,F3,G3,H3 \n A2,B2,C2,D2,E2,F2,G2,H2 \n A1,B1,C1,D1,E1,F1,G1,H1 
    #bishop moving from C6 to D5:
    pawn_positions_before = ",,,,,,, \n ,,B,,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,,",
    pawn_positions_after = ",,,,,,, \n ,,,,,,, \n ,,,B,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,,",
    author = player1,
    game = game1

)

#test with illegal board (it is hard to determine from string length whether board is legal because pawns will be dissapearing):
movement2 = Movement(
    #game_id should be extracted from game that will be defined
    #following mapping used: 
    #A8,B8,C8,D8,E8,F8,G8,H8 \n A7,B7,C7,D7,E7,F7,G7,H7 \n A6,B6,C6,D6,E6,F6,G6,H6 \n A5,B5,C5,D5,E5,F5,G5,H5 \n A4,B4,C4,D4,E4,F4,G4,H4 \n A3,B3,C3,D3,E3,F3,G3,H3 \n A2,B2,C2,D2,E2,F2,G2,H2 \n A1,B1,C1,D1,E1,F1,G1,H1 
    #bishop moving from C6 to D5:
    pawn_positions_before = ",,,,,,, \n ,,B,,,,, \n ,,,,,,, \n ,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,,",
    pawn_positions_after = ",,,,,,, \n ,,,,,,, \n ,,,B,,,, \n ,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,, \n ,,,,,,,",
    author = player1,
    game = game1

)

#we're going to leave game logic verification outside database.

#test to add correct movement
game1.movements.append(movement1)

#test to add some random string
game1.movements.append("random")