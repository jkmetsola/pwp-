*Database script*

This database will be generated using python script named app.py

*Required libraries*

The Python script uses Flask library to generate the database.
If you don't have this library on you computer, you can install it by running following commands in cmd:

pip install flask
pip install flask_sqlalchemy

After running this command, the required flask library will be installed on your computer.

*Used database*

The database type we will be using in this work is SQlite
in order to observe the structure of the database, you need to download and install SQlite3.
The SQlite3 is found from: https://www.sqlite.org/download.html
Recommended version to download is: sqlite-tools-win32-x86-3340100.zip

*Setting up the database*

In general situation, the database will be populated once the chess game is about to start.
However, there is a test function "testdb" in the script app.py which can be used to populated
the database which allows to test that the tables are set up correctly.

After running this fucntion in app.py, there should be stored data in the database which you can
observe using SQlite terminal.